This contains the capability to create a website using flask that makes
a game where you have to guess the name of a randomly selected picture
of a pokemon. It provides both coloured and silohouetted images of the
pokemon, the option of which given to the user. There is also the choice
of only having pokemon from specific games having the option of being
randomly chosen.

NO COPYRIGHT PERMISSIONS - NOT FOR DISTRIBUTION!