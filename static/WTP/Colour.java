import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import javax.imageio.ImageIO;

public class Colour
{
  public static void main(String[] args) throws Exception 
  {
    BufferedImage img = colorImage(ImageIO.read(new File("bulbasaur.png")));
    ImageIO.write(img, "png", new File("Test.png"));
  }

  private static BufferedImage colorImage(BufferedImage image)
  {
    int width = image.getWidth();
    int height = image.getHeight();
    WritableRaster raster = image.getRaster();

    for (int col = 0; col < width; col++) 
    {
      for (int row = 0; row < height; row++) 
      {
        int[] pixels = raster.getPixel(col, row, (int[]) null);
        if(pixels[0] > 0 && pixels[1] >0 && pixels[2]>0)
        {
        	pixels[0] = 0;
        	pixels[1] = 0;
        	pixels[2] = 0;
        	raster.setPixel(col, row, pixels);
        }	
      }
    }
    return image;
  }
}