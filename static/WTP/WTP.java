import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
public class WTP
{
	public static void main(String[] args) throws Exception
	{
		while(true)
		{
			int a=0;
			int b=0;
			int c=0;
			int d=0;
			int e=0;
			int f=0;
			int g=0;
			int h=0;
			int num = 0;
			String[] pokedex = new String[0];
			Scanner sc = new Scanner(System.in);
			System.out.println("What generation/s do you want? Type a number 1-7 or all for every generation.");
			while(true)
			{
				if (h>0)
				{
					System.out.println("Any other generations? Type 1-7 or no.");
				}
				String genNum = sc.next();
				if (genNum.equals("1"))
				{
					if(a>0)
					{
						System.out.println("Please type an unchosen generation or type no.");
						continue;
					}
					num = num+151;
					pokedex = Image.joinArrayGeneric(pokedex,Image.create(genNum));
					a++;
				}
				else if (genNum.equals("2"))
				{
					if(b>0)
					{
						System.out.println("Please type an unchosen generation or type no.");
						continue;
					}
					num = num+100;
					pokedex = Image.joinArrayGeneric(pokedex,Image.create(genNum));
					b++;
				}
				else if (genNum.equals("3"))
				{
					if(c>0)
					{
						System.out.println("Please type an unchosen generation or type no.");
						continue;
					}
					num = num+135;
					pokedex = Image.joinArrayGeneric(pokedex,Image.create(genNum));
					c++;
				}
				else if (genNum.equals("4"))
				{
					if(d>0)
					{
						System.out.println("Please type an unchosen generation or type no.");
						continue;
					}
					num = num+107;
					pokedex = Image.joinArrayGeneric(pokedex,Image.create(genNum));
					d++;
				}
				else if (genNum.equals("5"))
				{
					if(e>0)
					{
						System.out.println("Please type an unchosen generation or type no.");
						continue;
					}
					num = num+156;
					pokedex = Image.joinArrayGeneric(pokedex,Image.create(genNum));
					e++;
				}
				else if (genNum.equals("6"))
				{
					if(f>0)
					{
						System.out.println("Please type an unchosen generation or type no.");
						continue;
					}
					num = num+72;
					pokedex = Image.joinArrayGeneric(pokedex,Image.create(genNum));
					f++;
				}
				else if (genNum.equals("7"))
				{
					if(g>0)
					{
						System.out.println("Please type an unchosen generation or type no.");
						continue;
					}
					num = num+86;
					pokedex = Image.joinArrayGeneric(pokedex,Image.create(genNum));
					g++;
				}
				else if (genNum.equalsIgnoreCase("no"))
				{
					break;
				}
				else
				{
					num = num+807;
					pokedex = Image.joinArrayGeneric(pokedex,Image.create(genNum));
					break;
				}
				if (a>0 && b>0 && c>0 && d>0 && e>0 && f>0 && g>0)
				{
					break;
				}
				h++;
			}
			String poke = Image.random(pokedex,num);
			JFrame frame = new JFrame();
			System.out.println("Would you like coloured or silhouetted pictures? Type C or S respectively.");
			String pic = sc.next();
			for(int i=0;i<pokedex.length;i++)
			{
				Image.darken(pokedex[i]);
			}
			if (pic.equalsIgnoreCase("S"))
			{
				Image.darken(poke);
				ImageIcon icon = new ImageIcon(poke + "_new.png");
				JLabel ran_poke = new JLabel(icon);
				frame.add(ran_poke);
			}
			else
			{
				ImageIcon icon = new ImageIcon(poke + ".png");
				JLabel ran_poke = new JLabel(icon);
				frame.add(ran_poke);
			}
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.pack();
			frame.setVisible(true);
			System.out.println("Who's that Pokemon?");
			String answer = sc.next();
			JFrame frame_new = new JFrame();
			if (answer.equalsIgnoreCase(poke))
			{
				frame.dispose();
				System.out.println("You got it!");
				ImageIcon icon_new = new ImageIcon(poke + ".png");
				JLabel ran_poke_new = new JLabel(icon_new);
				frame_new.add(ran_poke_new);
				frame_new.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame_new.pack();
				frame_new.setVisible(true);
			}
			else
			{
				frame.dispose();
				System.out.println("Sadly no. The correct answer is " + poke);
				//JFrame frame_new = new JFrame();
				ImageIcon icon_new = new ImageIcon(poke + ".png");
				JLabel ran_poke_new = new JLabel(icon_new);
				frame_new.add(ran_poke_new);
				frame_new.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame_new.pack();
				frame_new.setVisible(true);
			}
			System.out.println("Would you like to play again? Y for yes, N for no.");
			String end = sc.next();
			if (end.equalsIgnoreCase("Y"))
			{
				frame_new.dispose();
			}
			else
			{
				frame_new.dispose();
				break;
			}
		}
	}
}
